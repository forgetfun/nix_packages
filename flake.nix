{
	description = "{{project_name}}";

	inputs = {
		flake-utils.url      = "github:numtide/flake-utils";
		nixpkgs.url          = "github:nixos/nixpkgs/nixos-23.05";
	};

	outputs = { flake-utils, nixpkgs, ... }: let
		overlay = final: prev: {
			forgetfun = final.lib.recurseIntoAttrs (final.callPackage ./pkgs/default.nix {});
		};
	in {
		overlays.default = overlay;
	} // flake-utils.lib.eachDefaultSystem (system:
			let
				pkgs = import nixpkgs { inherit system; overlays = [ overlay ]; };
			in {
				packages = removeAttrs pkgs.forgetfun [ "recurseForDerivations" "overrideDerivation" "override" ];
			}
		);
}
