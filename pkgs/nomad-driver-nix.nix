{ lib, buildGoModule, fetchFromGitHub }:

buildGoModule rec {
	pname = "nomad-driver-nix";
	version = "2022.02.19.001";

	src = fetchFromGitHub {
		owner = "input-output-hk";
		repo = pname;
		rev = "0e948009adfa1d9c96886e935dc901f32c443f30";
		sha256 = "sha256-mSELWkW+PSCuS2j7Li2NFNhXlveaH8n5tKz/C5kljbY=";
	};

	vendorSha256 = "sha256-FDJpbNtcFEHnZvWip2pvUHF3BFyfcSohrr/3nk9YS24=";

	subPackages = [ "." ];

	CGO_ENABLED = "0";
	GOOS = "linux";

	ldflags = [
		"-s"
		"-w"
		"-extldflags"
		"-static"
		"-X github.com/input-output-hk/nomad-driver-nix/nix.pluginVersion=${version}"
	];

	postInstall = ''
		mv $out/bin/nomad-driver-nix $out/bin/nix-driver
	'';
}
