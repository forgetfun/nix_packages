{ python3Packages, lib, fetchzip }:

python3Packages.buildPythonApplication rec {
  pname = "nerd-font-patcher";
  version = "4.5.1";

  src = fetchzip {
	url = "https://github.com/ryanoasis/nerd-fonts/raw/35259124bd42527d179d671c2f1d5ec9d23f6ac5/FontPatcher.zip";
    sha256 = "sha256-AcXZ951vhteozWK7znGH84J2T0WdgT8CR5vXNfN72UY=";
    stripRoot = false;
  };

  propagatedBuildInputs = with python3Packages; [ fontforge ];

  format = "other";

  postPatch = ''
    sed -i font-patcher \
      -e 's,__dir__ + "/src,"'$out'/share/,'
    sed -i font-patcher \
      -e  's,/bin/scripts/name_parser,/../lib/name_parser,'
  '';
  # Note: we cannot use $out for second substitution

  dontBuild = true;

  installPhase = ''
    mkdir -p $out/bin $out/share $out/lib
    install -Dm755 font-patcher $out/bin/nerd-font-patcher
    cp -ra src/glyphs $out/share/
    cp -ra bin/scripts/name_parser $out/lib/
  '';

  meta = with lib; {
    description = "Font patcher to generate Nerd font";
    homepage = "https://nerdfonts.com/";
    license = licenses.mit;
    maintainers = with maintainers; [ ck3d ];
  };
}
