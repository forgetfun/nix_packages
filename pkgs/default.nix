{ newScope }: let
	pkgs = let
		callPackage = newScope pkgs;
	in {
		edk2 = callPackage ./edk2/default.nix {};
		kickstart = callPackage ./kickstart.nix {};
		nerd-font-patcher_4 = callPackage ./nerd-font-patcher.nix {};
		nomad-driver-nix = callPackage ./nomad-driver-nix.nix {};
		quicksand = callPackage ./fonts/quicksand.nix {};
		velocity-bin = callPackage ./velocity-bin.nix {};
		volts = callPackage ./volts.nix {};
		minecraft-nanolimbo = callPackage ./minecraft-nanolimbo.nix {};
		inherit (callPackage ./fonts/iosevka/default.nix {}) iosevka-polarised-term;
	};
in pkgs
