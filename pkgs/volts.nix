{ lib
, rustPlatform
, fetchCrate
, installShellFiles
, pkg-config
, openssl
}:

rustPlatform.buildRustPackage rec {
	pname = "volts";
	version = "0.2.1";

	src = fetchCrate {
		inherit version;
		crateName = "volts";
		hash = "sha256-OiUMxNcMCs73YjpGHaiK5FYfbpOkeJIQPhdH6rN0VJs=";
	};

	cargoSha256 = "sha256-d7laMnlNRhCW+ug+HgX1AG97oBkHHmPoyixzMCuvLw8=";

	buildInputs = [ openssl ];
	nativeBuildInputs = [ pkg-config ];

	meta = with lib; {
		description = "Cli tool for publishing and managing Lapce plugins";
		homepage = "https://github.com/lapce/lapce-volts";
		license = with licenses; [ asl20 ];
		maintainers = with maintainers; [];
	};
}
