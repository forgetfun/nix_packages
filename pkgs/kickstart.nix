{ lib
, rustPlatform
, fetchFromGitHub
, pkg-config
, stdenv
, darwin
, git
}: rustPlatform.buildRustPackage rec {
	pname = "kickstart";
	version = "0.4.0";

	src = fetchFromGitHub {
		owner = "Keats";
		repo = "kickstart";
		rev = "v${version}";
		sha256 = "sha256-GIBSHPIUq+skTx5k+94/K1FJ30BCboWPA6GadgXwp+I=";
	};

	cargoSha256 = "sha256-cOcldEte7zxyxzvj7v7uCczs5AQ+v4mMfqmTK9hrv1o=";

	nativeBuildInputs = [ pkg-config ];

	buildInputs = [] ++ lib.optionals stdenv.isDarwin [
		darwin.apple_sdk.frameworks.Security
	];

	nativeCheckInputs = [ git ];

	checkFlags = [
		# require network access #
		"--skip=generation::tests::can_generate_from_remote_repo"
		"--skip=generation::tests::can_generate_from_remote_repo_with_subdir"
	];

	meta = with lib; {
		description = " A scaffolding tool to get new projects up and running quickly";
		longDescription = ''
			A CLI tool to easily get a new project up and running by using pre-made templates.
			This is a slightly more powerful version of an equivalent tool in Python, cookiecutter.
			It is an alternative to NodeJS projects such as Yeoman or Slush.
		'';
		homepage = "https://github.com/Keats/kickstart";
		license = licenses.mit;
		maintainers = [];
		platforms = platforms.all;
	};
}
